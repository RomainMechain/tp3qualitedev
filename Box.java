import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import com.google.gson.Gson;

class Thing{
    private String name;
    private int volume;
    /**
     * Constructeur de la classe Thing
     * @param name String le nom de thing
     */
    public Thing(String name){
        this.name = name;
        this.volume = 0;
    }
    /**
     * Constructeur de la classe Thing 
     * @param volume int le volume de thing
     */
    public Thing(int volume) {
        this.name = " ";
        this.volume = volume;
    }
    /**
     * Constructeur de la classe Thing 
     * @param name String le nom de thing
     * @param volume int le volume de thing 
     */
    public Thing(String name, int volume) {
        this.name = name;
        this.volume = volume;
    }

    /**
     * Methode qui permet d'obtenir le nom de Thing
     * @return String le nom de thing
     */
    public String getNomThing(){
        return this.name;

    }
    /**
     * Methode qui permet d'obtenir le volume de thing 
     * @return int le volume de thing
     */
    public int volume() {
        return this.volume;
    }

    /**
     * Methode qui permet de changer le nom de Thing
     * @param name String le nouveau nom de thing
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Methode qui retourne si le nom passé en paramètre correspond à celui de Thing 
     * @param nom String le nom à verfier 
     * @return Boolean true si ils sont egaux 
     */
    public boolean hasName (String nom){
        return this.name.equals(nom);
    }
}

class Box{
    private ArrayList<Thing> contents;
    private boolean ouverture;
    private int capacite;

    /**
     * Constructeur de la classe Box
     */
    public Box(){
        this.contents = new ArrayList<Thing>();
        this.ouverture = true;
        this.capacite = -1;
    }

    /**
     * Constructeur de la classe Box
     * @param ouverture boolean si la boite est ouverte ou non 
     * @param capacite int la capacité de la boite 
     */
    public Box(boolean ouverture, int capacite){
        this.contents = new ArrayList<Thing>();
        this.ouverture = ouverture;
        this.capacite = capacite;
    }
    
    /**
     * Methode qui ajoute un objet dans la boite 
     * @param truc l'objet à ajouter 
     */
    public void add(String truc) {
        this.contents.add(new Thing(truc));
    }

    public boolean estDedans(String mot) {
        for (Thing obj : this.contents) {
            if(obj.hasName(mot)) {
                return true;
            }
        }
        return false;
    }

    public boolean isOpen(){
        return this.ouverture;
    }

    public void close(){
        this.ouverture = false;
    }

    public void open(){
        this.ouverture = true;
    }

    public String actionLook(){
        String res = "La boîte contient: ";
        if (this.ouverture){
            for (Thing obj : this.contents){
                res = res+obj.getNomThing()+"| ";
            }
       }
        else{
            res = "La boîte est fermée";
       }
       return res;
    }

    public void setCapacity(int capaciteBoite){
        this.capacite = capaciteBoite;
    }

    public int capacity(){
        return this.capacite;
    }

    public boolean hasRoomFor(Thing objet) {
        if (this.capacite < objet.volume()) {
            return false;
        }
        return true;

    }

    public void actionAdd(Thing objet) throws PlaceException, CloseException{
        if (this.isOpen()){
            if (hasRoomFor(objet)){
                this.contents.add(objet);
                this.capacite = this.capacite - objet.volume(); 
            }
            else{
                throw new PlaceException();
            }
        }
        else{
            throw new CloseException();
        }
    }

    public Thing find(String objet) throws VideException, CloseException {
        if (this.isOpen()) {
            Thing obj = null;
            for(Thing thing : this.contents) {
                if (thing.hasName(objet)) {
                    obj = thing;
                }
            }
            if(obj == null) {
                throw new VideException();
            }
            return obj;
        }
        else{
            throw new CloseException();
        }
    }

    public String toJSON() {
        Gson gson = new Gson();
        return gson.toJson(new Box(this.ouverture,this.capacite));
    }

    public Box fromJSON() throws FileNotFoundException {
        Gson gson = new Gson();
        FileReader fr = new FileReader("fichier.json");
        return gson.fromJson(fr,Box.class);
    }

}
