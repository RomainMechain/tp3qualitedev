import org.junit.*;
import static org.junit.Assert.assertEquals;

import java.beans.Transient;
import java.io.FileNotFoundException;


public class TestsBoxes {
    @Test
    public void testBoxesCreate() {
        Box b = new Box();
    }
    /** On veut pouvoir mettre des trucs dedans */
    @Test
    public void testBoxAdd() {
        Box b = new Box();
        b.add("truc1");
        b.add("truc2");
    }

    @Test(expected = ArithmeticException.class)
    public void divisionWithException() {
    int i = 1/0;
    }

    @Test
    public void testOuverture() {
        Box uneBoite = new Box();   
        assertEquals(uneBoite.isOpen(),true);
        uneBoite.close();
        assertEquals(uneBoite.isOpen(),false);
        uneBoite.open();
        assertEquals(uneBoite.isOpen(),true);  
    }

    @Test
    public void testContenance(){
        Box boite = new Box();
        boite.add("truc1");
        boite.add("truc2");
        assertEquals(boite.actionLook(),"La boîte contient: truc1| truc2| ");
        Box autreBoite = new Box();
        autreBoite.close();
        assertEquals(autreBoite.actionLook(),"La boîte est fermée");
    }

    @Test
    public void testVolume(){
        Thing obj1 = new Thing(2);
        Thing obj2 = new Thing("obj2");
        Thing obj3 = new Thing("obj3", 3);
        assertEquals(obj1.volume(),2);
    }

    @Test
    public void testCapacite(){
        Box boite = new Box();
        assertEquals(boite.capacity(),-1);
        boite.setCapacity(9);
        assertEquals(boite.capacity(),9);
    }

    @Test
    public void testActionAdd() throws CloseException , PlaceException{
        Box boite = new Box();
        boite.setCapacity(9);
        Thing objet = new Thing("ARMAGEDDON DANS TA FACE", 8000);
        assertEquals(boite.capacity(),9);
        System.out.println(boite.actionLook());
        Thing objetbis = new Thing("chat mignon", 3);
        boite.actionAdd(objetbis);
        assertEquals(boite.capacity(),6);
        System.out.println(boite.actionLook());
    }

    @Test
    public void testHasRoomFor() {
        Box boite1 = new Box();
        boite1.setCapacity(9);
        Thing obj1 = new Thing(5);
        assertEquals(boite1.hasRoomFor(obj1),true);
        Thing obj2 = new Thing(15);
        assertEquals(boite1.hasRoomFor(obj2),false);
    }

    @Test
    public void testsetNameAndHasName(){
    Box boite1 = new Box();
    Thing obj1 = new Thing(5);
    obj1.setName("un truc normal");
    assertEquals(obj1.hasName("un truc normal"),true);
    }

    @Test 
    public void testFind() throws VideException, CloseException, PlaceException {
        Box boite1 = new Box();
        boite1.setCapacity(50);
        Thing obj = new Thing("obj", 40);
        boite1.actionAdd(obj);
        assertEquals(boite1.find("obj").getNomThing(),"obj");
    }

    @Test
    public void testToJson() throws CloseException , PlaceException {
        Box boite1 = new Box(true,45);
        System.out.println(boite1.toJSON());
        Thing t1 = new Thing("Un objet",5);
        boite1.actionAdd(t1);
        System.out.println(boite1.toJSON());
        System.out.println(boite1.actionLook());
    }

    @Test
    public void fromJson() throws FileNotFoundException {
        Box boite = new Box();
        Box boite2 = boite.fromJSON();
        System.out.println("Test from Json :"+boite2.capacity());
    }
}
